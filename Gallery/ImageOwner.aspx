﻿<%@ Page Language="C#" Inherits="Gallery.ImageOwner" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>ImageOwner</title>
    <style>
        body {
            background-color: #ECE996;
            font-size: large;
        }
    </style>    
</head>
<body>
     <form id="formImage" runat="server">
        <asp:Image id="galleryImage" runat="server"></asp:Image><br><br>
        <asp:Button id="returnButton" Text="Return to main page" OnClick="returnButtonClicked" runat="server"></asp:Button><br><br>
        <asp:Button id="galleryButton" Text="Return to gallery of owner" OnClick="galleryButtonClicked" runat="server"></asp:Button><br>
    </form>
</body>
</html>
