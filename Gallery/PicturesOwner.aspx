﻿<%@ Page Language="C#" Inherits="Gallery.PicturerOwner" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>PicturesOwner</title>
    <style>
        body {
            background-color: #ECE996;
            font-size: large;
        }
    </style>
</head>
<body>
	<form id="PicturesForm" runat="server">
        <asp:FileUpload id="fileUpload" runat="server">
        </asp:FileUpload><br><br>
        <asp:Button id="saveButton" runat="server" Text="Save" OnClick="saveButtonClicked"/><br><br>
        <asp:Table ID="pictureTable" runat="server" BorderColor="Black" BorderWidth="2px">
        </asp:Table>
        <asp:Label id="informLabel" 
             Text="" 
             runat="server"/><br><br>
        <asp:Label id="descrLabel" Text="Description" runat="server"/><br><br>
        <input type="text" name="description"/><br><br>
        <asp:Button id="returnButton" runat="server" Text="Return to main mage" OnClick="returnButtonClicked"/><br><br>
        <asp:Button id="ownerButton" runat="server" Text="Return to gallery page of owner" OnClick="ownerButtonClicked"/><br>
	</form>
</body>
</html>
