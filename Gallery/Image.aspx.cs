﻿using System;
using System.Web;
using System.Web.UI;
using MySql.Data.MySqlClient;

namespace Gallery
{

    public partial class Images : System.Web.UI.Page
    {

        /* row to connect with database */
        string connectionRow;
        /* constants for connection */
        const string server = "127.0.0.1";
        const string database = "gallery";
        const string user = "go";
        const string password = "Moskow2012";
        const string port = "3306";
        const string sslM = "none";
        /* image record in database */
        ImageRecord imageRecord = new ImageRecord();

        public void Page_Load(object sender, EventArgs args)
        {

            /* make row for connection */
            connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5};", server, port, user, password, database, sslM);
            /* receive id from past page */
            string id = Request.QueryString["id"];

            using (MySqlConnection connection = new MySqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* make request to get list of gallery */
                string queryGet = "select * from pictureList where id = " + id;
                MySqlCommand commGet = new MySqlCommand(queryGet, connection);
                MySqlDataReader reader = commGet.ExecuteReader();

                /* save result of reading in object */
                reader.Read();
                imageRecord.Id = Convert.ToInt32(reader[0].ToString());
                imageRecord.IdGall = Convert.ToInt32(reader[1].ToString());
                imageRecord.Descrption = reader[2].ToString();
                imageRecord.RedPath = reader[3].ToString();
                imageRecord.FullPath = reader[4].ToString();

            }

            /* add image on the website */
            galleryImage.ImageUrl = imageRecord.FullPath;
            informLabel.Text = Request.Url.AbsoluteUri;

            /* make button invisible during clicking */
            returnButton.OnClientClick = "this.style.visibility='hidden'";
            galleryButton.OnClientClick = "this.style.visibility='hidden'";

        }

        public void returnButtonClicked(object sender, EventArgs args)
        {
            /* create row for redirecting */
            string redirect = "Default.aspx";
            /* launch main page */
            Response.Redirect(redirect);
        }

        public void galleryButtonClicked(object sender, EventArgs args)
        {
            /* create row for redirecting */
            string redirect = string.Format("Pictures.aspx?id={0}", imageRecord.IdGall.ToString());
            /* launch gallery page */
            Response.Redirect(redirect);
        }

    }
}
