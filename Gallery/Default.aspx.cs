﻿using System;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Gallery
{

    public struct GalleryRecord
    {
        public int Id;
        public int GalleryNumber;
        public string Description;
        public string PathFolderRed;
        public string PathFolderFull;
    }

    public partial class Default : System.Web.UI.Page
    {

        /* row to connect with database */
        string connectionRow;
        /* constants for connection */
        const string server = "127.0.0.1";
        const string database = "gallery";
        const string user = "go";
        const string password = "Moskow2012";
        const string port = "3306";
        const string sslM = "none";
        /* array of buttons to open images */
        List<Button> openButtons = new List<Button>();
        /* login and password of owner */
        const string loginOwner = "bridgeArchitect";
        const string passwordOwner = "London2012";
        /* records of gallery from database */
        List<GalleryRecord> galleryRecords = new List<GalleryRecord>();
        /* delegate to sort records */
        public Comparison<GalleryRecord> FunctionCompare = CompareRecords;
        /* table of gallery */
        protected System.Web.UI.WebControls.Table tableGallery;
        /* label for information */
        protected System.Web.UI.WebControls.Label informLabel;

        /* fucntion to compare gallery records */
        public static int CompareRecords(GalleryRecord record1, GalleryRecord record2)
        {
            return record1.GalleryNumber - record2.GalleryNumber;
        }

        public void Page_Load(object sender, EventArgs args)
        {

            /* make row for connection */
            connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5};", server, port, user, password, database, sslM);

            using (MySqlConnection connection = new MySqlConnection(connectionRow))
            { 

                /* make connection */
                connection.Open();

                /* make request to get list of gallery */
                string queryGet = "select * from galleryList ";
                MySqlCommand commGet = new MySqlCommand(queryGet, connection);
                MySqlDataReader reader = commGet.ExecuteReader();

                /* read answer from database */
                while (reader.Read())
                {
                    /* creation objects for new record */
                    GalleryRecord galleryRecord = new GalleryRecord();

                    /* save result into list */
                    galleryRecord.Id = Convert.ToInt32(reader[0].ToString());
                    galleryRecord.GalleryNumber = Convert.ToInt32(reader[1].ToString().Split(':')[0]);
                    galleryRecord.Description = reader[1].ToString().Split(':')[1];
                    galleryRecords.Add(galleryRecord);
                }

                /* sort records using gallery number */
                galleryRecords.Sort(FunctionCompare);

                /* create new table */
                int i = 0;
                while (i < galleryRecords.Count)
                {
                    /* creation objects for new row */
                    TableRow curRow = new TableRow();
                    TableCell curIdCell = new TableCell();
                    TableCell curNumberCell = new TableCell();
                    TableCell curNameCell = new TableCell();
                    TableCell curOpenButtCell = new TableCell();
                    Button openButton = new Button();

                    /* give names and event handlers for buttons and
                       add to list after that */
                    openButton.Text = "Open";
                    openButton.OnClientClick = "this.style.visibility='hidden'";
                    openButton.Click += openButtonClicked;
                    openButtons.Add(openButton);

                    /* filling of cells */
                    curIdCell.Text = galleryRecords[i].Id.ToString();
                    curNumberCell.Text = galleryRecords[i].GalleryNumber.ToString();
                    curNameCell.Text = galleryRecords[i].Description;
                    curOpenButtCell.Controls.Add(openButton);

                    /* filling of row */
                    curRow.Cells.Add(curIdCell);
                    curRow.Cells.Add(curNumberCell);
                    curRow.Cells.Add(curNameCell);
                    curRow.Cells.Add(curOpenButtCell);

                    /* filling of table */
                    tableGallery.Rows.Add(curRow);
                    i++;
                }
            }

            /* make login button invisible during clicking */
            loginButton.OnClientClick = "this.style.visibility='hidden'";

        }

        public void openButtonClicked(object sender, EventArgs args) 
        {

            Button curButton = (Button)sender;
            int i = 0, idGallery = -1;
            /* find out sender button in the list */
            while (i < openButtons.Count)
            {
                if (curButton == openButtons[i])
                {
                    idGallery = galleryRecords[i].Id;
                    break;
                }
                i++;
            }

            /* to check search of button */
            if (idGallery != -1)
            {
                /* create row for redirecting */
                string redirect = string.Format("Pictures.aspx?id={0}", idGallery.ToString());
                /* launch new page to show pictures */
                Response.Redirect(redirect);
            }

        }

        public void loginButtonClicked(object sender, EventArgs args)
        {

            /* receive login and password from page */
            string loginPage = Request.Form["login"];
            string passwordPage = Request.Form["password"];

            if ((loginPage == loginOwner) && (passwordPage == passwordOwner))
            {
                /* create row for redirecting */
                string redirect = "Owner.aspx";
                /* launch new page to modify gallery */
                Response.Redirect(redirect);
                /* make empty information label */
                informLabel.Text = "";
            }
            else
            {
                /* write error into information label */
                informLabel.Text = "This user does not exist";
            }

        }

    }
}
