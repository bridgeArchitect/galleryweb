﻿<%@ Page Language="C#" Inherits="Gallery.Pictures" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Pictures</title>
    <style>
        body {
            background-color: #ECE996;
            font-size: large;
        }
    </style>
</head>
<body>
	<form id="pictureForm" runat="server">
        <asp:Table ID="pictureTable" runat="server" BorderColor="Black" BorderWidth="2px">
        </asp:Table><br><br>
        <asp:Button id="returnButton" Text="Return to main page" OnClick="returnButtonClicked" runat="server"></asp:Button><br>
	</form>
</body>
</html>
