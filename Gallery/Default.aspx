﻿<%@ Page Language="C#" Inherits="Gallery.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Main Page</title>
    <style>
        body {
            background-color: #ECE996;
            font-size: large;
        }
    </style>
</head>
<body>
	<form id="mainForm" runat="server">
        <asp:Table ID="tableGallery" runat="server" BorderColor="Black" BorderWidth="2px">
             <asp:TableRow>
                    <asp:TableCell>ID</asp:TableCell>
                    <asp:TableCell>Gallery Number</asp:TableCell>
                    <asp:TableCell>Description</asp:TableCell>
                    <asp:TableCell>Action</asp:TableCell>
             </asp:TableRow>
        </asp:Table><br><br>
		<asp:Button id="loginButton" runat="server" Text="Login" OnClick="loginButtonClicked"/><br><br>
        <asp:Label id="loginLabel" 
             Text="Login" 
             runat="server"/><br><br>
        <input type="text" name="login"/><br><br>
        <asp:Label id="passwordLabel" 
             Text="Password" 
             runat="server"/><br><br>
        <input type="text" name="password"/><br>
        <asp:Label id="informLabel" 
             Text="" 
             runat="server"/><br>
	</form>
</body>
</html>
