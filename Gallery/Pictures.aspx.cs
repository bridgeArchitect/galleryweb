﻿using System;
using System.Web;
using System.Web.UI;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Gallery
{

    /* record about image */
    public struct ImageRecord
    {
        public int Id;
        public int IdGall;
        public string Descrption;
        public string RedPath;
        public string FullPath;
        public int PictureNumber;
    }

    public partial class Pictures : System.Web.UI.Page
    {

        /* row to connect with database */
        string connectionRow;
        /* constants for connection */
        const string server = "127.0.0.1";
        const string database = "gallery";
        const string user = "go";
        const string password = "Moskow2012";
        const string port = "3306";
        const string sslM = "none";
        /* list of image records from database */
        List<ImageRecord> imagesList = new List<ImageRecord>();
        /* list of images */
        List<ImageButton> images = new List<ImageButton>();
        /* delegate to sort records */
        public Comparison<ImageRecord> FunctionCompare = CompareRecords;

        /* fucntion to compare image records */
        public static int CompareRecords(ImageRecord record1, ImageRecord record2)
        {
            return record1.PictureNumber - record2.PictureNumber;
        }

        public void Page_Load(object sender, EventArgs args)
        {

            /* make row for connection */
            connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5};", server, port, user, password, database, sslM);
            /* check redirecting */
            string path = Request.UrlReferrer.ToString();
            /* receive id from past page */
            string id = Request.QueryString["id"];

            using (MySqlConnection connection = new MySqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* make request to get list of gallery */
                string queryGet = "select * from pictureList where idGal = " + id;
                MySqlCommand commGet = new MySqlCommand(queryGet, connection);
                MySqlDataReader reader = commGet.ExecuteReader();

                /* read answer from database */
                while (reader.Read())
                {
                    /* save result of reading in object */
                    ImageRecord curImage = new ImageRecord();
                    curImage.Id = Convert.ToInt32(reader[0].ToString());
                    curImage.IdGall = Convert.ToInt32(reader[1].ToString());
                    curImage.Descrption = reader[2].ToString();
                    curImage.RedPath = reader[3].ToString();
                    curImage.FullPath = reader[4].ToString();
                    curImage.PictureNumber = Convert.ToInt32(reader[5].ToString());
                    /* add this image in the list */
                    imagesList.Add(curImage);
                }

            }

            /* sort records using picture number */
            imagesList.Sort(FunctionCompare);

            /* show images */
            int i = 0;
            TableRow pictureRow = new TableRow();
            while (i < imagesList.Count)
            {
                /* creation of image and cell */
                ImageButton curImage = new ImageButton();
                TableCell curPictCell = new TableCell();

                /* uploading image and adding to the cell */
                curImage.ImageUrl = imagesList[i].RedPath;
                curImage.ToolTip = imagesList[i].Descrption;
                curPictCell.Controls.Add(curImage);

                /* add handler to image and add image to list */
                curImage.Click += imageClicked;
                images.Add(curImage);
                
                /* adding cell to row */
                pictureRow.Cells.Add(curPictCell);
                i++;
            }

            /* adding row to table */
            pictureTable.Rows.Add(pictureRow);

            /* make return button invisible during clicking */
            returnButton.OnClientClick = "this.style.visibility='hidden'";

        }

        public void imageClicked(object sender, EventArgs args)
        {

            ImageButton currImage = (ImageButton)sender;
            int i = 0, idImage = -1;
            /* find out sender image in the list */
            while (i < images.Count)
            {
                if (images[i] == currImage)
                {
                    /* receive id of this image */
                    idImage = imagesList[i].Id;
                    break; 
                }
                i++;
            }

            /* to check search of image */
            if (idImage != -1)
            {
                /* create row for redirecting */
                string redirect = string.Format("Image.aspx?id={0}", idImage.ToString());
                /* launch new page to show one high-quality picture */
                Response.Redirect(redirect);
            }

        }

        public void returnButtonClicked(object sender, EventArgs args)
        {
            /* create row for redirecting */
            string redirect = "Default.aspx";
            /* launch main page */
            Response.Redirect(redirect);
        }

    }
}