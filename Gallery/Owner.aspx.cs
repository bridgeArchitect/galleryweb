﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Web.UI.WebControls;
using System.IO;
using System.Web;
using System.Web.UI;

namespace Gallery
{

    public partial class Owner : System.Web.UI.Page
    {

        /* gallery for photos */
        protected Table photoGallery;
        /* row to connect with database */
        string connectionRow;
        /* constants for connection */
        const string server = "127.0.0.1";
        const string database = "gallery";
        const string user = "go";
        const string password = "Moskow2012";
        const string port = "3306";
        const string sslM = "none";
        /* login and password of owner */
        const string loginOwner = "bridgeArchitect";
        const string passwordOwner = "London2012";
        /* path to folders of gallery */
        const string pathGallery = "/home/bridgearchitect/SP/Lab5/Gallery/Gallery";
        /* array of buttons to open images */
        List<Button> openButtons = new List<Button>();
        /* array of buttons to open gallary */
        List<Button> deleteButtons = new List<Button>();
        /* array of buttons to up gallary */
        List<Button> upButtons = new List<Button>();
        /* array of buttons to open gallary */
        List<Button> downButtons = new List<Button>();
        /* records of gallery from database */
        List<GalleryRecord> galleryRecords = new List<GalleryRecord>();
        /* delegate to sort records */
        public Comparison<GalleryRecord> FunctionCompare = CompareRecords;

        /* fucntion to compare gallery records */
        public static int CompareRecords(GalleryRecord record1, GalleryRecord record2)
        {
            return record1.GalleryNumber - record2.GalleryNumber;
        }

        public void clearAll()
        {
            /* clear all lists which are combine with this page */
            openButtons.Clear();
            deleteButtons.Clear();
            upButtons.Clear();
            downButtons.Clear();
            galleryRecords.Clear();
            photoGallery.Rows.Clear();
        }

        public void updateTable()
        {

            /* make row for connection */
            connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5};", server, port, user, password, database, sslM);
            /* clear all objects to work with new objects */
            clearAll();

            using (MySqlConnection connection = new MySqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* make request to get list of gallery */
                string queryGet = "select * from galleryList ";
                MySqlCommand commGet = new MySqlCommand(queryGet, connection);
                MySqlDataReader reader = commGet.ExecuteReader();

                /* read answer from database */
                while (reader.Read())
                {
                    /* creation objects for new record */
                    GalleryRecord galleryRecord = new GalleryRecord();

                    /* save result into list */
                    galleryRecord.Id = Convert.ToInt32(reader[0].ToString());
                    galleryRecord.GalleryNumber = Convert.ToInt32(reader[1].ToString().Split(':')[0]);
                    galleryRecord.Description = reader[1].ToString().Split(':')[1];
                    galleryRecord.PathFolderRed = reader[1].ToString().Split(':')[2];
                    galleryRecord.PathFolderFull = reader[1].ToString().Split(':')[3];
                    galleryRecords.Add(galleryRecord);
                }

                /* sort record using picture number */
                galleryRecords.Sort(FunctionCompare);

                /* create new table */

                /* creation objects for head row */
                TableRow headRow = new TableRow();
                TableCell headIdCell = new TableCell();
                TableCell headNumberCell = new TableCell();
                TableCell headNameCell = new TableCell();
                TableCell headOpenButtCell = new TableCell();
                TableCell headDelButtCell = new TableCell();
                TableCell headUpButtCell = new TableCell();
                TableCell headDownButtCell = new TableCell();

                /* filling of cells */
                headIdCell.Text = "Id";
                headNumberCell.Text = "Gallery number";
                headNameCell.Text = "Description";
                headOpenButtCell.Text = "Open";
                headDelButtCell.Text = "Delete";
                headUpButtCell.Text = "Up";
                headDownButtCell.Text = "Down";

                /* filling of row */
                headRow.Cells.Add(headIdCell);
                headRow.Cells.Add(headNumberCell);
                headRow.Cells.Add(headNameCell);
                headRow.Cells.Add(headOpenButtCell);
                headRow.Cells.Add(headDelButtCell);
                headRow.Cells.Add(headUpButtCell);
                headRow.Cells.Add(headDownButtCell);

                /* filling of table using head row */
                photoGallery.Rows.Add(headRow);

                /* create other rows */
                int i = 0;
                while (i < galleryRecords.Count)
                {
                    /* creation objects for new row */
                    TableRow curRow = new TableRow();
                    TableCell curIdCell = new TableCell();
                    TableCell curNumberCell = new TableCell();
                    TableCell curNameCell = new TableCell();
                    TableCell curOpenButtCell = new TableCell();
                    TableCell curDelButtCell = new TableCell();
                    TableCell curUpButtCell = new TableCell();
                    TableCell curDownButtCell = new TableCell();
                    Button openButton = new Button();
                    Button deleteButton = new Button();
                    Button upButton = new Button();
                    Button downButton = new Button();

                    /* give names and event handlers for buttons and
                       add to list after that */
                    openButton.Text = "Open";
                    openButton.Click += openButtonClicked;
                    openButton.OnClientClick = "this.style.visibility='hidden'";
                    openButtons.Add(openButton);
                    deleteButton.Text = "Delete";
                    deleteButton.Click += deleteButtonClicked;
                    deleteButton.OnClientClick = "this.style.visibility='hidden'";
                    deleteButtons.Add(deleteButton);
                    upButton.Text = "Up";
                    upButton.Click += upButtonClicked;
                    upButton.OnClientClick = "this.style.visibility='hidden'";
                    upButtons.Add(upButton);
                    downButton.Text = "Down";
                    downButton.Click += downButtonClicked;
                    downButton.OnClientClick = "this.style.visibility='hidden'";
                    downButtons.Add(downButton);

                    /* filling of cells */
                    curIdCell.Text = galleryRecords[i].Id.ToString();
                    curNumberCell.Text = galleryRecords[i].GalleryNumber.ToString();
                    curNameCell.Text = galleryRecords[i].Description;
                    curOpenButtCell.Controls.Add(openButton);
                    curDelButtCell.Controls.Add(deleteButton);
                    curUpButtCell.Controls.Add(upButton);
                    curDownButtCell.Controls.Add(downButton);

                    /* filling of row */
                    curRow.Cells.Add(curIdCell);
                    curRow.Cells.Add(curNumberCell);
                    curRow.Cells.Add(curNameCell);
                    curRow.Cells.Add(curOpenButtCell);
                    curRow.Cells.Add(curDelButtCell);
                    curRow.Cells.Add(curUpButtCell);
                    curRow.Cells.Add(curDownButtCell);

                    /* filling of table */
                    photoGallery.Rows.Add(curRow);
                    i++;
                }

            }

        }

        public void Page_Load(object sender, EventArgs args)
        {
            /* check redirecting */
            string path = Request.UrlReferrer.ToString();
            /* udpate table in website */
            updateTable();
            /* make return button invisible during clicking */
            returnButton.OnClientClick = "this.style.visibility='hidden'";
        }

        public void openButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idGallery = -1;
            /* find out sender button in the list */
            while (i < openButtons.Count)
            {
                if (curButton == openButtons[i])
                {
                    idGallery = galleryRecords[i].Id;
                    break;
                }
                i++;
            }

            /* to check search of button */
            if (idGallery != -1)
            {
                /* create row for redirecting */
                string redirect = string.Format("PicturesOwner.aspx?id={0}", idGallery.ToString());
                /* launch new page to show pictures */
                Response.Redirect(redirect);
            }

        }

        public void deleteButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idRow = -1;
            /* find out sender button in the list */
            while (i < deleteButtons.Count)
            {
                if (curButton == deleteButtons[i])
                {
                    idRow = i;
                    break;
                }
                i++;
            }

            if (idRow >= 0)
            {

                /* delete all files of this gallery */
                if (Directory.Exists(pathGallery + galleryRecords[idRow].PathFolderRed))
                {
                    Directory.Delete(pathGallery + galleryRecords[idRow].PathFolderRed, true);
                }
                if (Directory.Exists(pathGallery + galleryRecords[idRow].PathFolderFull))
                {
                    Directory.Delete(pathGallery + galleryRecords[idRow].PathFolderFull, true);
                }

                /* delete records from database */
                using (MySqlConnection connection = new MySqlConnection(connectionRow))
                {
                    /* open connection */
                    connection.Open();

                    /* make query to delete records from galleryList */
                    string queryDelete = "delete from galleryList where id = @id";
                    MySqlCommand commDelete = new MySqlCommand(queryDelete, connection);
                    commDelete.Parameters.Add("@id", MySqlDbType.Int32);
                    commDelete.Parameters["@id"].Value = galleryRecords[i].Id;

                    /* delete all records of this gallery from galleryList */
                    commDelete.ExecuteNonQuery();

                    /* make query to delete records from pictureList */
                    queryDelete = "delete from pictureList where idGal = @id";
                    commDelete = new MySqlCommand(queryDelete, connection);
                    commDelete.Parameters.Add("@id", MySqlDbType.Int32);
                    commDelete.Parameters["@id"].Value = galleryRecords[i].Id;

                    /* delete all records of this gallery from pictureList */
                    commDelete.ExecuteNonQuery();
                }

            }

            /* create row for redirecting */
            string redirect = "Owner.aspx";
            /* launch new page with updated table */
            Response.Redirect(redirect);

        }

        public void upButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idRow = -1;
            /* find out sender button in the list */
            while (i < upButtons.Count)
            {
                if (curButton == upButtons[i])
                {
                    idRow = i;
                    break;
                }
                i++;
            }

            if (idRow > 0)
            {
                /* initialization of records to change database */
                GalleryRecord currRecord = galleryRecords[idRow];
                GalleryRecord highRecord = galleryRecords[idRow - 1];

                using (MySqlConnection connection = new MySqlConnection(connectionRow))
                {
                    /* make connection */
                    connection.Open();

                    /* swap gallery number of two records */

                    /* make the first query */
                    string queryUpdate = "update galleryList set name=@name where id = @id";
                    MySqlCommand commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the current record */
                    commUpdate.Parameters.Add("@name", MySqlDbType.VarChar);
                    commUpdate.Parameters["@name"].Value = highRecord.GalleryNumber + ":" + currRecord.Description + ":" 
                    + currRecord.PathFolderRed + ":" + currRecord.PathFolderFull;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = currRecord.Id;
                    commUpdate.ExecuteNonQuery();

                    /* make the second query */
                    queryUpdate = "update galleryList set name=@name where id = @id";
                    commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the high record */
                    commUpdate.Parameters.Add("@name", MySqlDbType.VarChar);
                    commUpdate.Parameters["@name"].Value = currRecord.GalleryNumber + ":" + highRecord.Description + ":" 
                    + highRecord.PathFolderRed + ":" + highRecord.PathFolderFull;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = highRecord.Id;
                    commUpdate.ExecuteNonQuery();
                }

                /* create row for redirecting */
                string redirect = "Owner.aspx";
                /* launch new page with updated table */
                Response.Redirect(redirect);
            }


        }

        public void downButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idRow = downButtons.Count;
            /* find out sender button in the list */
            while (i < downButtons.Count)
            {
                if (curButton == downButtons[i])
                {
                    idRow = i;
                    break;
                }
                i++;
            }

            if (idRow < downButtons.Count - 1)
            {
                /* initialization of two record to change database */
                GalleryRecord currRecord = galleryRecords[idRow];
                GalleryRecord lowRecord = galleryRecords[idRow + 1];

                using (MySqlConnection connection = new MySqlConnection(connectionRow))
                {
                    /* make connection */
                    connection.Open();

                    /* swap gallery number of two records */

                    /* make the first query */
                    string queryUpdate = "update galleryList set name=@name where id = @id";
                    MySqlCommand commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the current record */
                    commUpdate.Parameters.Add("@name", MySqlDbType.VarChar);
                    commUpdate.Parameters["@name"].Value = lowRecord.GalleryNumber + ":" + currRecord.Description + ":" + 
                    currRecord.PathFolderRed + ":" + currRecord.PathFolderFull;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = currRecord.Id;
                    /* execute command */
                    commUpdate.ExecuteNonQuery();

                    /* make the second query */
                    queryUpdate = "update galleryList set name=@name where id = @id";
                    commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the low record */
                    commUpdate.Parameters.Add("@name", MySqlDbType.VarChar);
                    commUpdate.Parameters["@name"].Value = currRecord.GalleryNumber + ":" + lowRecord.Description + ":" +
                    lowRecord.PathFolderRed + ":" + lowRecord.PathFolderFull;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = lowRecord.Id;
                    /* execute command */
                    commUpdate.ExecuteNonQuery();
                }

                /* create row for redirecting */
                string redirect = "Owner.aspx";
                /* launch new page with updated table */
                Response.Redirect(redirect);
            }

        }

        public void returnButtonClicked(object sender, EventArgs args)
        {
            /* create row for redirecting */
            string redirect = "Default.aspx";
            /* launch gallery page */
            Response.Redirect(redirect);
        }

    }
}
