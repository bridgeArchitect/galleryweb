﻿<%@ Page Language="C#" Inherits="Gallery.Owner" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Owner</title>
    <style>
        body {
            background-color: #ECE996;
            font-size: large;
        }
    </style>
</head>
<body>
    <form id="ownerForm" runat="server">
        <asp:Table ID="photoGallery" runat="server" BorderColor="Black" BorderWidth="2px">
             <asp:TableRow>
                    <asp:TableCell>ID</asp:TableCell>
                    <asp:TableCell>Gallery Number</asp:TableCell>
                    <asp:TableCell>Description</asp:TableCell>
                    <asp:TableCell>Open</asp:TableCell>
                    <asp:TableCell>Delete</asp:TableCell>
                    <asp:TableCell>Up</asp:TableCell>
                    <asp:TableCell>Down</asp:TableCell>
             </asp:TableRow>
        </asp:Table><br>
        <asp:Button id="returnButton" runat="server" Text="Return to main mage" OnClick="returnButtonClicked"/><br>
        <asp:Label id="informLabel" 
             Text="" 
             runat="server"/><br>
    </form>
</body>
</html>
