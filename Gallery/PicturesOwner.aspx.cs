﻿using System;
using System.Web;
using System.Web.UI;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

namespace Gallery
{

    public partial class PicturerOwner : System.Web.UI.Page
    {
    
        /* row to connect with database */
        string connectionRow;
        /* constants for connection */
        const string server = "127.0.0.1";
        const string database = "gallery";
        const string user = "go";
        const string password = "Moskow2012";
        const string port = "3306";
        const string sslM = "none";
        int idGal;
        /* list of image records from database */
        List<ImageRecord> imagesList = new List<ImageRecord>();
        /* list of images */
        List<ImageButton> images = new List<ImageButton>();
        /* array of buttons to open gallary */
        List<Button> deleteButtons = new List<Button>();
        /* array of buttons to up gallary */
        List<Button> upButtons = new List<Button>();
        /* array of buttons to open gallary */
        List<Button> downButtons = new List<Button>();
        /* path to folders of gallery */
        const string pathGallery = "/home/bridgearchitect/SP/Lab5/Gallery/Gallery";
        /* delegate to sort records */
        public Comparison<ImageRecord> FunctionCompare = CompareRecords;

        /* fucntion to compare image records */
        public static int CompareRecords(ImageRecord record1, ImageRecord record2)
        {
            return record1.PictureNumber - record2.PictureNumber;
        }

        public static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
        {
            /* decrease image for showing */
            return (System.Drawing.Image)(new Bitmap(imgToResize, size));
        }

        public void clearAll()
        {
            /* clear lists */
            imagesList.Clear();
            images.Clear();
            pictureTable.Rows.Clear();
            deleteButtons.Clear();
            upButtons.Clear();
            downButtons.Clear();
        }

        public void updateTable()
        {

            /* clear lists in table */
            clearAll();
            /* make row for connection */
            connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5};", server, port, user, password, database, sslM);
            /* check redirecting */
            string path = Request.UrlReferrer.ToString();
            /* receive id from past page and save it */
            string id = Request.QueryString["id"];
            idGal = Convert.ToInt32(id);

            using (MySqlConnection connection = new MySqlConnection(connectionRow))
            {
                /* make connection */
                connection.Open();

                /* make request to get list of gallery */
                string queryGet = "select * from pictureList where idGal = " + id;
                MySqlCommand commGet = new MySqlCommand(queryGet, connection);
                MySqlDataReader reader = commGet.ExecuteReader();

                /* read answer from database */
                while (reader.Read())
                {
                    /* save result of reading in object */
                    ImageRecord curImage = new ImageRecord();
                    curImage.Id = Convert.ToInt32(reader[0].ToString());
                    curImage.IdGall = Convert.ToInt32(reader[1].ToString());
                    curImage.Descrption = reader[2].ToString();
                    curImage.RedPath = reader[3].ToString();
                    curImage.FullPath = reader[4].ToString();
                    curImage.PictureNumber = Convert.ToInt32(reader[5].ToString());
                    /* add this image in the list */
                    imagesList.Add(curImage);
                }
            }

            /* sort records using picture number */
            imagesList.Sort(FunctionCompare);

            /* show tables of images and controls */

            /* creation of row */
            TableRow pictureRow = new TableRow();
            TableRow deleteRow = new TableRow();
            TableRow upRow = new TableRow();
            TableRow downRow = new TableRow();
            int i = 0;
            while (i < imagesList.Count)
            {
                /* creation of image, controls and cells */
                ImageButton curImage = new ImageButton();
                Button deleteButton = new Button();
                Button upButton = new Button();
                Button downButton = new Button();
                TableCell curPictCell = new TableCell();
                TableCell curDeleteCell = new TableCell();
                TableCell curUpCell = new TableCell();
                TableCell curDownCell = new TableCell();

                /* uploading image and controls */
                curImage.ImageUrl = imagesList[i].RedPath;
                curImage.ToolTip = imagesList[i].Descrption;
                deleteButton.Text = "Delete";
                upButton.Text = "Up";
                downButton.Text = "Down";

                /* give image and controls to cells */
                curPictCell.Controls.Add(curImage);
                curDeleteCell.Controls.Add(deleteButton);
                curUpCell.Controls.Add(upButton);
                curDownCell.Controls.Add(downButton);

                /* add handler to image */
                curImage.Click += imageClicked;
                deleteButton.Click += deleteButtonClicked;
                upButton.Click += upButtonClicked;
                downButton.Click += downButtonClicked;

                /* and add image and controls to list */
                images.Add(curImage);
                deleteButtons.Add(deleteButton);
                upButtons.Add(upButton);
                downButtons.Add(downButton);

                /* adding cells to rows */
                pictureRow.Cells.Add(curPictCell);
                deleteRow.Cells.Add(curDeleteCell);
                upRow.Cells.Add(curUpCell);
                downRow.Cells.Add(curDownCell);

                i++;
            }

            /* adding row to table */
            pictureTable.Rows.Add(pictureRow);
            pictureTable.Rows.Add(deleteRow);
            pictureTable.Rows.Add(upRow);
            pictureTable.Rows.Add(downRow);

        }

        public void Page_Load(object sender, EventArgs args)
        {
            /* check redirecting */
            string path = Request.UrlReferrer.ToString();
            /* update image table */
            updateTable();
            /* make button invisible during clicking */
            returnButton.OnClientClick = "this.style.visibility='hidden'";
            ownerButton.OnClientClick = "this.style.visibility='hidden'";
        }

        public void imageClicked(object sender, EventArgs args)
        {

            ImageButton currImage = (ImageButton)sender;
            int i = 0, idImage = -1;
            /* find out sender image in the list */
            while (i < images.Count)
            {
                if (images[i] == currImage)
                {
                    /* receive id of this image */
                    idImage = imagesList[i].Id;
                    break;
                }
                i++;
            }

            /* to check search of image */
            if (idImage != -1)
            {
                /* create row for redirecting */
                string redirect = string.Format("ImageOwner.aspx?id={0}", idImage.ToString());
                /* launch new page to show one high-quality picture */
                Response.Redirect(redirect);
            }

        }

        public void deleteButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idColumn = -1, idPicture = -1;
            /* find out sender button in the list */
            while (i < deleteButtons.Count)
            {
                if (curButton == deleteButtons[i])
                {
                    idPicture = imagesList[i].Id;
                    idColumn = i;
                    break;
                }
                i++;
            }

            if (idPicture != -1)
            {

                /* receive path to images */
                string filepathFull = pathGallery + imagesList[idColumn].FullPath.Split('~')[1];
                string filepathRed = pathGallery + imagesList[idColumn].RedPath.Split('~')[1];

                /* delete image from file system */
                if (File.Exists(filepathFull))
                {
                    File.Delete(filepathFull);
                }
                if (File.Exists(filepathRed))
                {
                    File.Delete(filepathRed);
                }

                /* delete record about image in database */
                using (MySqlConnection connection = new MySqlConnection(connectionRow))
                {
                    /* open connection */
                    connection.Open();

                    /* make query to delete record from pictureList */
                    string queryDelete = "delete from pictureList where id = @id";
                    MySqlCommand commDelete = new MySqlCommand(queryDelete, connection);
                    commDelete.Parameters.Add("@id", MySqlDbType.Int32);
                    commDelete.Parameters["@id"].Value = idPicture;

                    /* delete all records of this gallery from galleryList */
                    commDelete.ExecuteNonQuery();
                }
            }

            /* receive id from current page */
            string id = Request.QueryString["id"];
            /* create row for redirecting */
            string redirect = string.Format("PicturesOwner.aspx?id={0}", id);
            /* launch new page with updated table */
            Response.Redirect(redirect);

        }

        public void upButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idCol = -1;
            /* find out sender button in the list */
            while (i < upButtons.Count)
            {
                if (curButton == upButtons[i])
                {
                    idCol = i;
                    break;
                }
                i++;
            }

            if (idCol > 0)
            {
                /* initialization of record to change database */
                ImageRecord currRecord = imagesList[idCol];
                ImageRecord highRecord = imagesList[idCol - 1];

                using (MySqlConnection connection = new MySqlConnection(connectionRow))
                {
                    /* make connection */
                    connection.Open();

                    /* swap picture number of two records */

                    /* make the first query */
                    string queryUpdate = "update pictureList set pictureNumber=@number where id = @id";
                    MySqlCommand commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the current record */
                    commUpdate.Parameters.Add("@number", MySqlDbType.Int32);
                    commUpdate.Parameters["@number"].Value = highRecord.PictureNumber;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = currRecord.Id;
                    /* execure command */
                    commUpdate.ExecuteNonQuery();

                    /* make the second query */
                    queryUpdate = "update pictureList set pictureNumber=@number where id = @id";
                    commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the high record */
                    commUpdate.Parameters.Add("@number", MySqlDbType.Int32);
                    commUpdate.Parameters["@number"].Value = currRecord.PictureNumber;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = highRecord.Id;
                    /* execute command */
                    commUpdate.ExecuteNonQuery();
                }

                /* receive id from current page */
                string id = Request.QueryString["id"];
                /* create row for redirecting */
                string redirect = string.Format("PicturesOwner.aspx?id={0}", id);
                /* launch new page with updated table */
                Response.Redirect(redirect);

            }

        }

        public void downButtonClicked(object sender, EventArgs args)
        {

            Button curButton = (Button)sender;
            int i = 0, idCol = downButtons.Count;
            /* find out sender button in the list */
            while (i < downButtons.Count)
            {
                if (curButton == downButtons[i])
                {
                    idCol = i;
                    break;
                }
                i++;
            }

            if (idCol < downButtons.Count - 1)
            {
                /* initialization */
                ImageRecord currRecord = imagesList[idCol];
                ImageRecord lowRecord = imagesList[idCol + 1];

                using (MySqlConnection connection = new MySqlConnection(connectionRow))
                {
                    /* make connection */
                    connection.Open();

                    /* make the first query */
                    string queryUpdate = "update pictureList set pictureNumber=@number where id = @id";
                    MySqlCommand commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the current record */
                    commUpdate.Parameters.Add("@number", MySqlDbType.Int32);
                    commUpdate.Parameters["@number"].Value = lowRecord.PictureNumber;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = currRecord.Id;
                    /* execure command */
                    commUpdate.ExecuteNonQuery();

                    /* make the second query */
                    queryUpdate = "update pictureList set pictureNumber=@number where id = @id";
                    commUpdate = new MySqlCommand(queryUpdate, connection);

                    /* change gallery number for the high record */
                    commUpdate.Parameters.Add("@number", MySqlDbType.Int32);
                    commUpdate.Parameters["@number"].Value = currRecord.PictureNumber;
                    commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
                    commUpdate.Parameters["@id"].Value = lowRecord.Id;
                    /* execute command */
                    commUpdate.ExecuteNonQuery();
                }

                /* receive id from current page */
                string id = Request.QueryString["id"];
                /* create row for redirecting */
                string redirect = string.Format("PicturesOwner.aspx?id={0}", id);
                /* launch new page with updated table */
                Response.Redirect(redirect);
            }

        }

        public void saveButtonClicked(object sender, EventArgs args)
        {

            using (MySqlConnection connection = new MySqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* make request to path gallery */
                string queryGet = "select * from galleryList where id=" + idGal.ToString();
                MySqlCommand commGet = new MySqlCommand(queryGet, connection);
                MySqlDataReader reader = commGet.ExecuteReader();

                /* read one record */
                reader.Read();
                /* creation objects for new record */
                GalleryRecord galleryRecord = new GalleryRecord();

                /* save result into object */
                galleryRecord.Id = Convert.ToInt32(reader[0].ToString());
                galleryRecord.GalleryNumber = Convert.ToInt32(reader[1].ToString().Split(':')[0]);
                galleryRecord.Description = reader[1].ToString().Split(':')[1];
                galleryRecord.PathFolderRed = reader[1].ToString().Split(':')[2];
                galleryRecord.PathFolderFull = reader[1].ToString().Split(':')[3];

                /* close reader */
                reader.Close();

                /* prexif of filaneme */
                string prefixFileRed = galleryRecord.PathFolderRed.Split('/')[2];
                string prefixFileFull = galleryRecord.PathFolderFull.Split('/')[2];
                string filepathFull = "";
                string filepathRed = "";

                /* save full file from source */
                int num = 1;
                while (true)
                {
                    filepathFull = pathGallery + galleryRecord.PathFolderFull + "/" + prefixFileFull + num.ToString() + ".jpg";
                    filepathRed = pathGallery + galleryRecord.PathFolderRed + "/" + prefixFileRed + num.ToString() + ".jpg";
                    /* check whether file exists */
                    if (!File.Exists(filepathFull))
                    {
                        fileUpload.SaveAs(filepathFull);
                        break;
                    }
                    num++;
                }

                /* resize source image and save */
                System.Drawing.Image imageFull = System.Drawing.Image.FromFile(filepathFull);
                System.Drawing.Image imageRed = resizeImage(imageFull, new Size(200, 150));
                imageRed.Save(filepathRed);

                /* receive description from page */
                string descriptionPage = Request.Form["description"];

                /* make query to insert new record for database */
                string queryAdd = "insert into pictureList (idGal,name,redPath,fullPath,pictureNumber) values(@idGal,@name,@redPath,@fullPath,@pictureNumber)";
                /* create command to execute */
                MySqlCommand commAdd = new MySqlCommand(queryAdd, connection);

                /* add all paramaters for request */
                commAdd.Parameters.Add("@idGal", MySqlDbType.Int32);
                commAdd.Parameters["@idGal"].Value = idGal;
                commAdd.Parameters.Add("@name", MySqlDbType.VarChar);
                commAdd.Parameters["@name"].Value = descriptionPage;
                commAdd.Parameters.Add("@redPath", MySqlDbType.VarChar);
                commAdd.Parameters["@redPath"].Value = "~" + galleryRecord.PathFolderRed + "/" + prefixFileRed + num.ToString() + ".jpg";
                commAdd.Parameters.Add("@fullPath", MySqlDbType.VarChar);
                commAdd.Parameters["@fullPath"].Value = "~" + galleryRecord.PathFolderFull + "/" + prefixFileFull + num.ToString() + ".jpg";
                commAdd.Parameters.Add("@pictureNumber", MySqlDbType.Int32);
                commAdd.Parameters["@pictureNumber"].Value = imagesList[imagesList.Count - 1].PictureNumber + 1;

                /* execute command to add new image */
                commAdd.ExecuteNonQuery();

            }

            /* receive id from current page */
            string id = Request.QueryString["id"];
            /* create row for redirecting */
            string redirect = string.Format("PicturesOwner.aspx?id={0}", id);
            /* launch new page with updated table */
            Response.Redirect(redirect);

        }

        public void returnButtonClicked(object sender, EventArgs args)
        {
            /* create row for redirecting */
            string redirect = "Default.aspx";
            /* launch gallery page */
            Response.Redirect(redirect);
        }

        public void ownerButtonClicked(object sender, EventArgs args)
        {
            /* create row for redirecting */
            string redirect = "Owner.aspx";
            /* launch gallery page */
            Response.Redirect(redirect);
        }

    }

}
